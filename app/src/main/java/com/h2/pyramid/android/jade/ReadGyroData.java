package com.h2.pyramid.android.jade;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;


public class ReadGyroData extends Service implements SensorEventListener{

    private SensorManager mSensorManager;
    public float gx,gy;
    public double AVsvm;
    public long OnGyroDataReceived;
    public long OnGyroCalcDone;
    IBinder mBinder;
    @Override
    public void onCreate() {

        mSensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_UI);

    }
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        gx = event.values[0];
        gy = event.values[1];
        OnGyroDataReceived = System.nanoTime();
        CovAVsvm(gx,gy);
    }

    private void CovAVsvm(float gx1, float gy1){
        double rad2deg = (180/Math.PI);
        double dgx1 = (Double.parseDouble(new Float(gx1).toString()))*rad2deg;
        double dgy1 = (Double.parseDouble(new Float(gy1).toString()))*rad2deg;


        AVsvm =  Math.sqrt((dgx1*dgx1)+(dgy1*dgy1));

        if(AVsvm > 170)
        {
            OnGyroCalcDone = System.nanoTime();
            System.out.println("Angular Velocity Norm" + AVsvm);
            broadcastIntent();
        }

    }

    private void broadcastIntent(){
        Intent intent = new Intent();
        intent.setAction("com.h2.pyramid.android.jade.gyro");
        intent.putExtra("OnGyroDataReceived", OnGyroDataReceived);
        intent.putExtra("OnGyroCalcDone", OnGyroCalcDone);
        sendBroadcast(intent);
    }
}
