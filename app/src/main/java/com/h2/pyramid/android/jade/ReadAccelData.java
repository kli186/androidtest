package com.h2.pyramid.android.jade;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class ReadAccelData extends Activity implements SensorEventListener {
	

	public double ax,ay,az;
	public double a_norm, m_angle; // added m_angle.
	public int i=0;
	static int BUFF_SIZE=50;
	static public double[] window = new double[BUFF_SIZE];
	static public double[] angle = new double[BUFF_SIZE]; // added for angle
	double sigma=0.5,th=10,th1=5,th2=2;
	double twog = 2*9.81; // added
	public boolean AVsvm = false; // added, angular velocity norm, should be in double(degree).
	public boolean ACCsvm = false; // added, accel norm.
	public boolean fall = false; // added
	private SensorManager sensorManager;
	public static String curr_state,prev_state;
	public MediaPlayer m1_fall,m2_sit,m3_stand,m4_walk;
	public long OnGyroDataReceived, OnGyroCalcDone;

	BroadcastReceiver QueryGyroReceiver;

	public class MyReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			AVsvm = true;
			Integer processing = 0;
			synchronized (processing) {

					OnGyroDataReceived = intent.getLongExtra("OnGyroDataReceived", OnGyroDataReceived);
					OnGyroCalcDone = intent.getLongExtra("OnGyroCalcDone", OnGyroCalcDone);
					System.out.println("AccelBroadCastReceived: "+System.nanoTime());
					System.out.println("GyroDataReceived:  "+OnGyroDataReceived);
					System.out.println("GyroCalcDone: "+OnGyroCalcDone);
					ReadAccelData.this.FallDetection(window, angle);

			}

		}

	}


	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accel_main);
        sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);

		IntentFilter filter = new IntentFilter();
		filter.addAction("com.h2.pyramid.android.jade.gyro");
		QueryGyroReceiver = new MyReceiver();
		registerReceiver(QueryGyroReceiver, filter);

        initialize();
    }
	private void initialize() {
		// TODO Auto-generated method stub
		 for(i=0;i<BUFF_SIZE;i++){
	    	 window[i]=0;
	     }
		 prev_state="none";
		 curr_state="none";
		 m1_fall=MediaPlayer.create(getBaseContext(), R.raw.fall);
	     m2_sit=MediaPlayer.create(getBaseContext(), R.raw.sitting);
	     m3_stand=MediaPlayer.create(getBaseContext(), R.raw.standing);
	     m4_walk=MediaPlayer.create(getBaseContext(), R.raw.walking);
	}
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
	}

	@SuppressLint("ParserError")
	@Override
   	public void onSensorChanged(SensorEvent event) {

		if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
				ax=event.values[0];
				ay=event.values[1];
				az=event.values[2];
				AddData(ax,ay,az);
				mAngle(ax,ay,az);
				posture_recognition(window,ay);
				FallDetection(window,angle);
				SystemState(curr_state,prev_state, fall); // added fall
			   if(!prev_state.equalsIgnoreCase(curr_state)){
					prev_state=curr_state;
				}
		}
   	}
	private void posture_recognition(double[] window2,double ay2) {
		// TODO Auto-generated method stub
		int zrc=compute_zrc(window2);
		if(zrc==0){ 
			
			if(Math.abs(ay2)<th1){
				curr_state="sitting";
			}else{
				curr_state="standing";
			}
				
		}else{
			
			if(zrc>th2){
				curr_state="walking";
			}else{
				curr_state="none";
			}
		}
	}

	private int compute_zrc(double[] window2) {
		// TODO Auto-generated method stub
		int count=0;
		for(i=1;i<=BUFF_SIZE-1;i++){
			
			if((window2[i]-th)<sigma && (window2[i-1]-th)>sigma){
				count=count+1;
			}
		}
		return count;
	}

	private void FallDetection(double[] window2, double[] angle2)
	{
		/*
		double maxima = window2[0];
		double minima = window2[0];
		for(int i = 1; i<=BUFF_SIZE-1;i++) {
			if (window2[i]<minima) {
				minima = window2[i];
			}
			if (window2[i] > maxima) {
				maxima = window2[i];
			}
		}
		if((maxima - minima)>twog ) {*/

		if(AVsvm == true){
			for(int i =0;i<BUFF_SIZE-1;i++){
				if(window2[i]>twog)
				{
					ACCsvm = true;
				}
			}

			if(ACCsvm == true) {
				for (int i = 0; i < BUFF_SIZE - 1; i++) { //should be 30-100 samples, stated from the research, but using 50 this time.
					if (angle2[i] > 60) {
						fall = true;
					}
				}
			}

		}
	}
	private void SystemState(String curr_state1,String prev_state1, boolean fall1) {
		// TODO Auto-generated method stub

	        	//none = transition state
              if(!prev_state1.equalsIgnoreCase(curr_state1)){
            	  if((curr_state1.equalsIgnoreCase("none") && fall1) || (curr_state1.equalsIgnoreCase("sitting") && (fall1))){
            		  m1_fall.start();
					  TextView textView = (TextView) findViewById(R.id.textView01);
					  textView.setText("FALL");
					  System.out.println("FALL");
					  fall = false;
					  AVsvm = false;
					  ACCsvm = false;
            	  }
            	  if((curr_state1.equalsIgnoreCase("sitting") && !fall1)){
            		  m2_sit.start();
					  TextView textView = (TextView) findViewById(R.id.textView01);
					  textView.setText("SITTING");
            	  }
            	  if((curr_state1.equalsIgnoreCase("standing")&& !fall1)){
            		  m3_stand.start();
					  TextView textView = (TextView) findViewById(R.id.textView01);
					  textView.setText("STANDING");
            	  }
            	  if((curr_state1.equalsIgnoreCase("walking") && !fall1)){
            		  m4_walk.start();
					  TextView textView = (TextView) findViewById(R.id.textView01);
					  textView.setText("WALKING");
            	  }
              }
	}
	private void AddData(double ax2, double ay2, double az2) {
		// TODO Auto-generated method stub
		 a_norm=Math.sqrt((ax2*ax2)+(ay2*ay2)+(az2*az2));
		 for(i=0;i<=BUFF_SIZE-2;i++){
	    	window[i]=window[i+1];
	     }
	     window[BUFF_SIZE-1]=a_norm;
	       
	}
	private void mAngle(double ax3, double ay3, double az3){
		m_angle = (Math.atan((Math.sqrt((ay3*ay3)+(az3*az3)))/(ax3*ax3)))*(180/Math.PI);
		for(i=0;i<=BUFF_SIZE-2;i++){
			angle[i] =angle[i+1];
		}
		angle[BUFF_SIZE-1] = m_angle;
	}

	
	public void exit_app(View view){
		   finish();
	}
	   
       
	
}
